<!DOCTYPE html>
<head>
    <title> Mass shooting data </title>
</head>
<body>
    <?
    $lastData = "2019-08-05";
    
    $files = array(
        "massshootingtrackerorg_2013.csv",
        "massshootingtrackerorg_2014.csv",
        "massshootingtrackerorg_2015.csv",
        "massshootingtrackerorg_2016.csv",
        "massshootingtrackerorg_2017.csv",
        "massshootingtrackerorg_2018.csv",
        "massshootingtrackerorg_2019-08-05.csv"
    );
    
    function ReadCsvFile( $file )
    {
        if ( !file_exists( $file ) )
        {
            echo( "Couldn't find file " . $file );
        }
        
        $csvload = array_map('str_getcsv', file( $file ));
        array_walk( $csvload, function(&$a) use ( $csvload ) {
          $a = array_combine( $csvload[0], $a );
        });
        // array_shift($csv); # remove column header
        
        return $csvload;
    }
    
    $stateCounts = array();
    $stateKilledCount = array();
    $stateWoundedCount = array();
    $killed = 0;
    $wounded = 0;
    
    $csv = array();
    $tempCsv = array();
    
    for ( $i = 0; $i < 7; $i++ )
    {
        $tempCsv = ReadCsvFile( "data/" . $files[$i] );

        $csv = array_merge( $csv, $tempCsv );
    }
    
    foreach ( $csv as $key => $value ) {
        if ( $key == 0 ) { continue; }
        $state = $value["state"];
        $killed += $value["killed"];
        $wounded += $value["wounded"];
        
        // Count how many each state has
        $state = strtoupper( $state );
        if ( array_key_exists( $state, $stateCounts ) )
        {
            $stateCounts[ $state ]++;
        }
        else
        {
            $stateCounts[ $state ] = 1;
        }
        
        if ( array_key_exists( $state, $stateWoundedCount ) )
        {
            $stateWoundedCount[ $state ]++;
        }
        else
        {
            $stateWoundedCount[ $state ] = 1;
        }
        
        if ( array_key_exists( $state, $stateKilledCount ) )
        {
            $stateKilledCount[ $state ]++;
        }
        else
        {
            $stateKilledCount[ $state ] = 1;
        }
    }
    
    ksort( $stateCounts );
    ?>
    
    <style type="text/css">
        table { width: 100%; overflow: scroll; }
        tr:nth-child(even) {background: #CCC}
        tr:nth-child(odd) {background: #FFF}
        
        td { border: solid 1px #aaa; }
        
        tr th { cursor: pointer; }
    </style>
    
    <h1>Mass shooting data</h1>
    
    <p>
        Data files are pulled from
        <a href="https://www.massshootingtracker.org/">https://www.massshootingtracker.org/</a>,
        last pulled <?=$lastData?>
    </p>
    
    <h2>Amount of incidents by state, 2013 to <?=$lastData?></h2>
    
    <table>
        <tr>
            <th>State</th>
            <? foreach( $stateCounts as $state => $count ) { ?>
                <th><?=$state?></th>
            <? } ?>
        </tr>
        <tr>
            <th>Incidents</th>
            <? foreach( $stateCounts as $state => $count ) { ?>
                <td><?=$count?></td>
            <? } ?>
        </tr>
        <tr>
            <th>Killed</th>
            <? foreach( $stateKilledCount as $state => $count ) { ?>
                <td><?=$count?></td>
            <? } ?>
        </tr>
        <tr>
            <th>Wounded</th>
            <? foreach( $stateWoundedCount as $state => $count ) { ?>
                <td><?=$count?></td>
            <? } ?>
        </tr>
    </table>
    
    <p><strong>Total incidents: </strong><?=sizeof( $csv ) ?></p>
    <p><strong>Total wounded: </strong><?=$wounded ?></p>
    <p><strong>Total killed: </strong><?=$killed ?></p>
    

    
    <script src='tablesort-5.0.2/src/tablesort.js'></script>

    <!-- Include sort types you need -->
    <script src='tablesort-5.0.2/src/sorts/tablesort.number.js'></script>
    <script src='tablesort-5.0.2/src/sorts/tablesort.date.js'></script>

    <script>
      new Tablesort(document.getElementById('ice-data'));
    </script>
</body>
