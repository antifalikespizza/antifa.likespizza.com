<!DOCTYPE html>

<html lang="en">

<? include_once( "backend.php" ); ?>

<head>
  <meta charset="utf-8">

  <title> Anti-Fascism Resources </title>
  <meta name="description" content="Resources to organize and fight fascism">
  <meta name="author" content="RWSS">

  <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- <script src="js/scripts.js"></script> -->
    <h1>Fascism Fighting Resources</h1>
    
    <p>
        <a href="#organize">Organize</a> |
        <a href="#projects">Projects</a> |
        <a href="#contact">Contact</a> |
	<a href="data/resources.json">resources.json</a> | 
        <a href="https://bitbucket.org/%7B0e9d7842-2441-4f40-b3ed-d0398ab44b7c%7D/">Project repositories</a>
    </p>
    
    <hr><a name="organize"><h2>Find people to organize with</h2></a>
    
    <p>Volunteer, donate, organize!</p>

    <h3>Specific regions</h3>
    
    <ul>
        <li><a href="kansas-city/index.php">Kansas City, Kansas/Missouri</a></li>
    </ul>

	<? foreach( $resourcesJson as $category => $data ) { ?>

	<? if ( sizeof( $data ) == 0 ) { continue; } ?>
	
	<a name="<?=$category?>"><h3><?=$category?></h3></a>

	<table>
		<tr><th>Group</th><th>Region</th><th>Website</th><th>Facebook</th></tr>
		<? foreach( $data as $index => $resource ) { ?>
			<tr>
				<td><?=$resource["name"]?></td>
				<td><?=$resource["location"]?></td>
				<td><?=$resource["website"]?></td>
				<td><?=$resources["facebook"]?></td>
			</tr>
		<? } ?>
	
	<? } ?>
	</table>

    <table>
        <tr>
            <th>Group</th><th>Region</th><th>Website</th><th>Facebook</th>
        </tr>
        
        <tr>
            <td>RAICES</td>
            <td>Texas</td>
            <td><a href="https://www.raicestexas.org">raicestexas.org</a></td>
            <td><a href="https://www.facebook.com/raicestexas/">raicestexas</a></td>
        </tr>
        
        <tr>
            <td>Lights for Liberty</td>
            <td>Various locations</td>
            <td><a href="https://www.lightsforliberty.org/?">lightsforliberty.org</a></td>
            <td></td>
        </tr>
        
        
        <tr>
            <td>IJAM, Immigrant Justice Advocacy Group</td>
            <td>Kansas City</td>
            <td><a href="http://www.ijamkc.org/">IJAMKC.ORG</a></td>
            <td><a href="https://www.facebook.com/IJAMKC/">IJAMKC</a></td>
        </tr>
    </table>
    
    <hr><a name="projects"><h2>Anti-fascism projects</h2></a>
    
    <ul>
        <li><a href="ice-facility-list/index.php">ICE detention centers in the U.S.</a></li>
        <li><a href="imass-shootings/index.php">Mass shooting data in the U.S.</a></li>
    </ul>
    
    <hr><a name="contact"><h2>Contact</h2></a>
    
    <p>rwss@likespizza.com</p>
    
</body>
</html>
