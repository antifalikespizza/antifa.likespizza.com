<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> Anti-Fascism Resources in Kansas City </title>
  <meta name="description" content="Resources to organize and fight fascism">
  <meta name="author" content="RWSS">

  <link rel="stylesheet" href="../style.css">

</head>

<body>
    <!-- <script src="js/scripts.js"></script> -->
    <h1>Fascism Fighting Resources - Kansas City</h1>
    
    <p>
<a href="http://antifa.likespizza.com/index.php"> &lt;&lt; All resources</a> |
<a href="https://bitbucket.org/%7B0e9d7842-2441-4f40-b3ed-d0398ab44b7c%7D/">Project repositories</a>
</p>
    
    <hr><h2>Find people to organize with</h2>
    
    
        
        <a name="general"><h3>General</h3></a>
        
        <table>
            <tr>
                <th>Group</th><th>Website</th><th>Facebook</th>
            </tr>

            <tr>
                <td>Resist - KC</td>
                <td><a href="http://www.resistkc.com/">resistkc.com (down)</a></td>
                <td><a href="https://www.facebook.com/resistKC/">@resistKC</a></td>
            </tr>
        </table>
    
        <a name="immigration"><h3>Immigration rights</h3></a>
        
        <table>
            <tr>
                <th>Group</th><th>Website</th><th>Facebook</th>
            </tr>
            
            <tr>
                <td>Lights for Liberty</td>
                <td><a href="https://www.lightsforliberty.org/?">lightsforliberty.org</a></td>
                <td><a href="https://www.facebook.com/lightsforlibertykc">Lights for Liberty - KC</a></td>
            </tr>
            
            <tr>
                <td>IJAM, Immigrant Justice Advocacy Group</td>
                <td><a href="http://www.ijamkc.org/">IJAMKC.ORG</a></td>
                <td><a href="https://www.facebook.com/IJAMKC/">IJAMKC</a></td>
            </tr>
            
            <tr>
                <td>KC Metro Immigration Alliance - KC MÍA</td>
                <td></td>
                <td><a href="https://www.facebook.com/KCMetroImmigrationAlliance/">KC Metro Immigration Alliance - KC MÍA</a></td>
            </tr> 
                       
            <tr>
                <td>SURJ KC - Showing Up for Racial Justice, Kansas City</td>
                <td></td>
                <td><a href="https://www.facebook.com/SURJKansasCity/">@SURJKansasCity</a></td>
            </tr>
            

        </table>
        
        <a name="reproductive"><h3>Reproductive Rights</h3></a>
        
        <table>
            <tr>
                <th>Group</th><th>Website</th><th>Facebook</th>
            </tr>
            
<!--
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
-->
            
            <tr>
                <td>NARAL Pro-Choice Missouri</td>
                <td><a href="https://prochoicemissouri.org/">prochoicemissouri.org</a></td>
                <td><a href="https://www.facebook.com/naralprochoicemissouri/">@naralprochoicemissouri</a></td>
            </tr>
        </table>


    
    <hr><a name="projects"><h2>Anti-fascism projects</h2></a>
    
    <ul>
        <li><a href="ice-facility-list/index.php">ICE detention centers in the U.S.</a></li>
    </ul>
</body>
</html>
